import React from 'react';
import { NavLink  } from 'react-router-dom';

const CategoryMemu = () => {
  const onMenuClick = () => {
    $('.hero__categories ul').slideToggle(400);
  };

  return (
    <div className="hero__categories">
      <div className="hero__categories__all" onClick={onMenuClick}>
        <i className="fa fa-bars" />
        <span>All Departments</span>
      </div>
      <ul>
        <li><NavLink to="/shop?category=1">Fresh Meat</NavLink></li>
        <li><NavLink to="/shop?category=2">Vegetables</NavLink></li>
        <li><NavLink to="/shop?category=3">Fruit & Nut Gifts</NavLink></li>
        <li><NavLink to="/shop?category=4">Fresh Berries</NavLink></li>
        <li><NavLink to="/shop?category=5">Ocean Foods</NavLink></li>
        <li><NavLink to="/shop?category=6">Eggs</NavLink></li>
        <li><NavLink to="/shop?category=7">Fastfood</NavLink></li>
        <li><NavLink to="/shop?category=8">Fresh Onion</NavLink></li>
        <li><NavLink to="/shop?category=9">Papayaya & Crisps</NavLink></li>
        <li><NavLink to="/shop?category=11">Fresh Bananas</NavLink></li>
      </ul>
    </div>
  );
};

export default CategoryMemu;
