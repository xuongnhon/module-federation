const remotes = {
  shared: {
    url: `http://localhost:${process.env.REACT_APP_SHARED_PORT || 3005}/remoteEntry.js`,
    scope: 'shared'
  }
};

export default remotes