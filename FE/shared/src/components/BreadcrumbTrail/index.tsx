import React from 'react';

import { createMountComponent } from '../../utils/mountComponent';

export interface IBreadcrumbTrailProps {
  title: string;
  current: string;
  breadcrumbItems: IBreadcrumbItem[];
  navigate: (path: string) => void;
}

export interface IBreadcrumbItem {
  title: string;
  path: string;
}

const BreadcrumbTrail = (props: IBreadcrumbTrailProps): JSX.Element => {
  return (
    <section className="breadcrumb-section set-bg" style={{ backgroundImage: 'url(/img/breadcrumb.jpg)' }}>
      <div className="container">
        <div className="row">
          <div className="col-lg-12 text-center">
            <div className="breadcrumb__text">
              <h2>{props.title}</h2>
              <div className="breadcrumb__option">
                {props.breadcrumbItems.map(breadcrumbItem => {
                  return (<a href='#' onClick={() => props.navigate(breadcrumbItem.path)} key={breadcrumbItem.path}>{breadcrumbItem.title}</a>)
                })}
                <span>{props.current}</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default (el: any, props: IBreadcrumbTrailProps) => createMountComponent(BreadcrumbTrail, props, el);
